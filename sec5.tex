\section{Spline Moment Equations for Transformed Equation}
\label{sec:5}
{%
We have described and analyzed how typical distribution functions can be approximated using a weighted FCS ansatz. In this section a set of PDEs for the evolution of the basis coefficients including the macroscopic values $\rho, v, \theta$ will be derived. The Boltzmann-BGK equation is transformed analogously to \cite{Koellermeier2014}. Then we use the aforementioned weighted FCS ansatz, perform a Galerkin projection and rewrite the resulting system in concise form.

Beginning with the one-dimensional Boltzmann-BGK equation
\begin{equation}
 \label{BoltzBKG}
 \dt f(t,x,c) + c\: \dx f(t,x,c) = S(f)  = \frac{1}{\tau}\left(\frac{\rho}{\sqrt{2 \pi \theta}} \exp\left(-\frac{(c-v)^2}{2\theta}\right)-f\right),
\end{equation}
we allow for an efficient discretization using spline functions by performing a non-linear transformation of the velocity space used in \cite{Kauf2011,Koellermeier2014}
\begin{equation}
\label{trans}
 \xi(x,t,c):=\frac{c-v(x,t)}{\sqrt{\theta(x,t)}}
\end{equation}
to obtain the following transformed equation denoting $f=f(t,x,\xi)$
\begin{multline}
\label{TransformedB}
 D_t f+\seta \xi \dx f + \partial_{\xi} f \left(-\frac{1}{\seta} (D_t v + \seta \xi \dx v) - \frac{1}{2\theta} \xi (D_t \theta + \seta \xi \dx \theta)\right) \\ =S(f,\rho,v,\theta) = \frac{1}{\tau}\left(\frac{\rho}{\sqrt{2 \pi \theta}} \exp(-\xi^2/2)-f\right),
\end{multline}
where $D_t:=\dt + v\:\dx$ denotes the convective time derivative.

Equation \eqref{TransformedB} indeed looks more complicated than the standard Boltzmann-BGK equation. However, we will see below that a concise system of equations can be derived from it in a straightforward way using the spline basis. The transformation \eqref{trans} makes it possible to use a very coarse grid in the velocity space. This grid typically only consists of a few spline basis functions. It is important to note that both $v(t,x)$ and $\theta(t,x)$ depend on the solution as moments of the distribution function and also depend on time and space. Due to the non-linearity of the transformation the ansatz in velocity space can be interpreted as using a moving grid in the velocity space, which is shifted by the velocity $v(t,x)$ and scaled by the variance $\sqrt{\theta(x,t)}$. This leads to a gain in efficiency and allows to achieve high accuracy with very few moments in comparison with standard schemes that simply expand around a global equilibrium function in the velocity space.

Following the literature, a scaled distribution function $\tilde f$ is used
\begin{equation}
\label{DefTildeF}
 \tilde f := \frac{\seta}{\rho}f,
\end{equation}
which leads to the following relations for moments of the scaled distribution function $\tilde f$
\begin{equation}
\label{one-o-one}
 \inftyint \tilde f(t,x,\xi)d\xi =1, \quad
 \inftyint \xi f(t,x,\xi)d\xi =0, \quad
 \inftyint \xi^2 f(t,x,\xi)d\xi =1.
\end{equation}
%\begin{equation}
%\label{one-o-one}
%\begin{split}
% \inftyint \tilde f(t,x,\xi)d\xi &=1, \\
% \inftyint \xi f(t,x,\xi)d\xi &=0,\\
% \inftyint (c-v)^2 f(t,x,\xi)d\xi &=1,
%\end{split}
%\end{equation}
and Equation \ref{TransformedB} then results in the transformed Boltzmann-BGK equation

\begin{equation}
 \begin{split}
\label{FinalB}
\left(\frac{1}{\rho} D_t \rho - \frac{d}{2\theta} D_t \theta \right) \tilde f + D_t \tilde f \\+\: \seta \xi \left( \left( \frac{1}{\rho} \dx \rho - \frac{d}{2\theta}\dx \theta \right) \tilde f + \dx \tilde f \right)  \\+\: \partial_{\xi} \tilde f \left( - \frac{1}{\seta}(D_t v + \seta \xi \dx v ) - \frac{1}{2 \theta} \xi (D_t \theta + \seta \xi \dx \theta ) \right) \\=\: \frac{1}{\tau}\left(\frac{\rho}{\sqrt{2 \pi \theta}} \exp(-\xi^2/2)-\frac{\rho}{\seta}\tilde f\right).
 \end{split}
\end{equation}

According to the previous sections, we now choose a weighted FCS ansatz for $\tilde f$ so that Equations \ref{one-o-one} are fulfilled.
\begin{equation}
 \tilde f = \frac{1}{\sqrt{2\pi}} \exp(-\xi^2/2) \left( 1+ \sum_{i=1}^{n-3} \kappa_{i} \phi_i \right).
\end{equation}
The full vector of unknowns is given by $\left(\rho, v, \theta, \kappa_1, \dots, \kappa_{n-3}\right) \in \mathbb{R}^{n}$.

For the Galerkin projection, the test functions include the first three monomials to reproduce the Euler equations, which are the conservation laws of mass, momentum, and energy
\begin{equation}
    \psi \in \{ 1, \xi, \xi^2 \} \cup (\hat \psi_j)_{j \in \{1,\ldots,n-3\}}
\end{equation}
and $(n-3)$ remaining FCS $\hat \psi_j$.

The resulting PDE system then reads
\begin{equation}
 \begin{split}
\label{angesetztB}
 \left( \frac{1}{\rho}D_t \rho - \frac{1}{2 \theta} D_t \theta \right) (M \kappa+V) + M D_t \kappa \\
 + \seta \left( \left( \frac{1}{\rho} \dx \rho - \frac{1}{2\theta} \dx \theta \right) (M^{\xi} \kappa + V^{\xi}) + M^{\xi} \dx \kappa \right) \\
 - \frac{1}{\seta}\left( D_t v (M^{\dxi} \kappa+V^{\dxi}) + \seta \dx v (M^{\xi \dxi} \kappa+V^{\xi \dxi}) \right)\\
 - \frac{1}{2 \theta}\left(D_t \theta (M^{\xi \dxi} \kappa+V^{\xi \dxi}) + \seta \dx \theta (M^{\xi \xi \dxi} \kappa+V^{\xi \xi \dxi}) \right)= \frac{1}{\tau} M \kappa
\end{split}
\end{equation}
{with} $n \times (n-3)$-{matrices} $M, M^{\xi}, M^{\dxi}, M^{\xi \dxi}$ and $M^{\xi \xi \dxi}$ as well as vectors $V$, $V^{\xi}$, $V^{\dxi}$, $V^{\xi \dxi}$ and $V^{\xi \xi \dxi}$ of length $n$.
{Denoting the scalar product} $\langle f,g \rangle:=\inftyint f(\xi)g(\xi) \; d\xi$ {and} $w=\frac{1}{\sqrt{2\pi}} \exp(-\xi^2/2)$ { the matrices and vectors can be written as}:
\begin{center}
$M_{ij}=\langle \psi_i,w \phi_j \rangle$, \quad
$V_{i}=\langle \psi_i,w \rangle$,\\
$M^{\xi}_{ij}=\langle \psi_i,\xi w\phi_j \rangle$, \quad
$V^{\xi}=\langle \psi_i,\xi w \rangle$,\\
$M^{\dxi}_{ij}=\langle \psi_i,\dxi(w\phi_j) \rangle$, \quad
$V^{\dxi}_{ij}=\langle \psi_i,\dxi w \rangle$,\\
$M^{\xi \dxi}_{ij}=\langle \psi_i,\xi \dxi (w\phi_j) \rangle$, \quad
$V^{\xi \dxi}_{ij}=\langle \psi_i,\xi \dxi w \rangle$, \\
$M^{\xi \xi \dxi}_{ij}=\langle \psi_i,\xi \xi \dxi (w\phi_j) \rangle$, \quad
{and} \quad
$V^{\xi \xi \dxi}_{ij}=\langle \psi_i,\xi \xi \dxi w \rangle$.
\end{center}
{%
In compact notation Equation \ref{angesetztB} can be written as
}
\begin{equation}
\label{ABsystem}
 B \: \dt  \begin{pmatrix}\rho\\v\\\theta\\\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix} + A \: \dx \begin{pmatrix}\rho\\v\\\theta\\\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix} = \frac{1}{\tau} M \begin{pmatrix}\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix},
\end{equation}
{%
where matrices $A$ and $B$ are defined as
}
\begin{equation}
 A=
 \begin{pmatrix}
    A_1 \: \vline \: A_2 \: \vline \: A_3 \: \vline \: v M + \seta M^{\xi}
   \end{pmatrix}
   \in \reals^{n\times n}
\end{equation}
{with}
\begin {gather}
 A_1=\frac{v}{\rho}\left(M \kappa +V\right)+\frac{\seta}{\rho} \left(M^{\xi} \kappa+ V^{\xi}\right), \\
 A_2=-\frac{v}{\seta}\left(M^{\dxi} \kappa + V^{\dxi}\right) - \left(M^{\xi \dxi} \kappa +V^{\xi \dxi}\right), \\
 A_3=-\frac{1}{2 \theta}\left(v \left(\left(M+M^{\xi \dxi}\right) \kappa +V+V^{\xi \dxi}\right)+ \seta \left(\left(M^{\xi}+M^{\xi \xi \dxi}\right) \kappa +V^{\xi}+V^{\xi \xi \dxi}\right)\right)
\end {gather}
{and}
\begin{equation}
 B=
 \begin{pmatrix}
  B_1 \: \vline \: B_2 \: \vline \: B_3 \: \vline \: M
 \end{pmatrix}
 \in \reals^{n \times n}
\end{equation}
{with}
\begin{gather}
 B_1=\frac{1}{\rho}\left(M \kappa + V\right), \\B_2=-\frac{1}{\seta}\left(M^{\dxi} \kappa +V^{\dxi}\right),\\
 B_3=-\frac{1}{2\theta}\left(\left(M+M^{\xi \dxi}\right) \kappa +V+V^{\xi \dxi}\right).
\end{gather}

{Because of} $B^{-1} \left(B_1 \: \vline \: B_2 \: \vline \: B_3 \: \vline \: M\right)=B^{-1}B=I_n$, {we know that}
$B^{-1} M =
\begin{pmatrix}
0 \\ I_{n-3}
\end{pmatrix}.
$

After multiplication with $B^{-1}$ Equation \ref{ABsystem} yields the final system of PDEs
\begin{equation}
    \label{AsysDGL}
    \dt  \begin{pmatrix}\rho\\v\\\theta\\\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix} + B^{-1}A \: \dx \begin{pmatrix}\rho\\v\\\theta\\\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix} = \frac{1}{\tau} \begin{pmatrix}0\\0\\0\\\kappa_1\\\vdots\\\kappa_{n-3}\end{pmatrix}.
\end{equation}

The specific entries of the system matrix $\asys(\rho,v,\kappa)=B^{-1} A$ can be easily computed after choosing the number of unknowns $n$ and a corresponding grid for the FCS. The system \eqref{AsysDGL} is then called Spline Moment Equations (SME).

\begin{remark}
    Due to choosing the first three test functions as $1, \xi, \xi^2$, the first three rows of Equation \ref{AsysDGL} are precisely the Euler equations
    \begin{equation}
        D_t \rho + \rho \dx v = 0, \quad D_t v + \frac{1}{\rho} \dx p =0, \quad D_t \theta + \frac{1}{\rho} \dx q + \frac{2p}{\rho} \dx v = 0
    \end{equation}
    with pressure tensor $p$ and heat flux $q$
    \begin{equation}\label{e:Q}
        p = \rho \theta \inftyint \xi^2 \tilde f d\xi, \quad q = \rho \theta^{(3/2)} \inftyint \xi^3 \tilde f d\xi,
    \end{equation}
    which can be verified by inserting the test functions ${1, \xi, \xi^2}$ into Equation \ref{angesetztB}.
\end{remark}

\begin{remark}
    Due to the transformation \ref{trans} the velocity space discretization becomes non-uniform as it depends on $v(x,t)$ and $\theta(x,t)$. This means that the spline functions are adaptively positioned around the mean velocity $v(x,t)$ and the range of their support scales with the temperature $\theta(x,t)$. The range of the support is then effectively $[v-\xi_{min}\sqrt{\theta},v+\xi_{max}\sqrt{\theta}]$. Different regions of the flow field with different $v(x,t)$ and $\theta(x,t)$ can thus be accurately approximated using one single basis with relatively few unknowns. A similar procedure for discrete velocity methods was used with $\xi_{min/max} = \pm 4$ in \cite{Brull2014}. As the same range led to good results for the approximation properties, in Section \ref{sec:4} we will make use of the same range for most of the next section.
\end{remark}


\subsection{Eigenvalues and Hyperbolicity}
We briefly consider the properties of the system matrix. Only if all eigenvalues of the system matrix are real-valued, the PDE system is globally hyperbolic and constitutes a meaningful physical model for the simulation of rarefied gases. Previously developed models using Hermite polynomials are not globally hyperbolic, see \cite{Koellermeier2014}, but there are some known procedures that yield global hyperbolicity, see \cite{Fan2016,Koellermeier2017b}.

We compute the eigenvalues for different numbers $(n-3)$ of fundamental constrained splines, for order $k=1$ and $\xi$-range $[\xi_{min}, \xi_{max}]=[-4,4]$.

Similarly as in \cite{Cai2013} and \cite{Koellermeier2014} all eigenvalues $\zeta_i$ of $\asys$ are shifted by the mean velocity $v$ and scaled with the square root of the temperature $\theta$, i.e.
\begin{equation}
\label{zeta}
 \zeta_i = v \pm \seta \beta_i \kappas.
\end{equation}

The eigenvalue $\zeta_i$ is thus real-valued if and only if $\beta_i \kappas$ is real. $\beta_i \kappas$ is the zero of the modified characteristic polynomial
\begin{equation}
 P(\lambda) = \det (\asys - (\lambda \seta +v)\:E) = 0.
\end{equation}

In Figure \ref{evhigh} we show the hyperbolicity area of the SME using different number of equations $n=5,7,9,11$, depending on the parameters $\kappa_i$ that correspond to the \emph{center} basis functions with an index close to $\frac{(n-3)}{2}$ while setting the outer kappas to zero. Hyperbolicity is only obtained in a small domain around the origin, which represents equilibrium. The hyperbolicity domain shrinks with increasing $n$. Furthermore, the hyperbolicity plots show striking similarity with the hyperbolicity plots for the Hermite-based ansatz in \cite{Cai2013}.%, which almost exactly reproduces Figure \ref{evunf1} for example.

\begin{figure}[H]
\centering
 \begin{subfigure}[b]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/2dplot/hyp1}
 \caption{$n=5$.}
 \label{evunf1}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/2dplot/hyp2}
 \caption{$n=7$.}
 \label{evunf2}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/2dplot/hyp3}
 \caption{$n=9$.}
 \label{evunf3}
 \end{subfigure}
 \begin{subfigure}[b]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/2dplot/hyp4}
 \caption{$n=11$.}
 \label{evunf4}
\end{subfigure}
\caption{{Hyperbolicity domain for varying $n$ depending on center $\kappa_{i/i+1}$.}}
\label{evhigh}
\end{figure}
{%
Similar results are obtained for different $n \in \{1,\ldots, 12\}$, support ranges $[\xi_{min}, \xi_{max}]$, and orders $k$, for which we have always observed bounded hyperbolicity domains due to the occurrence of imaginary eigenvalues for larger values of the coefficients.

The SME thus lack global hyperbolicity, similar to Grad's system \cite{Grad1949}.

\begin{remark}
    Despite the lack of hyperbolicity, the model is expected to give satisfactory results within the hyperbolic domain, just like Grad's model. We note that this paper focusses on the spline approximation itself and further research is necessary for the hyperbolicity of the model. For details on hyperbolic regularisation of different models, we refer to \cite{Fan2016,Koellermeier2014b}. As we do not focus on a hyperbolic regularization in this paper, we only consider a very basic possibility to achieve a hyperbolic PDE system in this work. It is based on a linearization around the equilibrium state, which is similar to the regularization in \cite{Cai2013} in a different variable setting. In Appendix \ref{app} we show the application of this linearized SME model yielding global hyperbolicity. Unfortunately, the simplifications of the linearization are too severe and the model does no longer converge to the solution of the Boltzmann-BGK equation. It is thus necessary to try one of the more advanced methods mentioned in the references above.
\end{remark} 