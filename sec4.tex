\section{Spline Approximations of Distribution Functions}
\label{sec:4}

Using B-splines and FCS, we investigate three possible ways for an expansion of the distribution function via splines called unweighted splines, weighted splines and weighted FCS. As test cases we will use selected bimodal functions, which often occur as distribution functions in kinetic equations.

Firstly, the distribution function will later be scaled and shifted in the velocity variable to a new velocity space $\xi$. The scaled distribution function then has $\rho=1$, $v=0$ and $\theta=1$, see Section \ref{sec:5}. We thus only consider examples of that form here.


Secondly, the ansatz should not be a mere linear combination of splines, due to the fact that the equilibrium function is a Maxwellian (or a Gaussian in the transformed space). Instead, we assume the distribution function to be a sum of a Gaussian distribution $\maboxinormalflat$ plus a linear combination of basis functions. The linear combination will thus ultimately describe the distribution function's \emph{deviation} from the Gaussian distribution, while a Maxwellian can be represented exactly by setting all basis coefficients to zero. This reduces the approximation error to zero in equilibrium.

Thirdly, to describe deviations from Maxwellians easily, also the basis functions can be weighted with that Maxwellian. To that extend, we include a weighting of the ansatz functions with the Gaussian distribution $\maboxinormalflat$ in the transformed space. This leads to suppressed oscillations towards the boundary of the discretization grid $\xi \in [\xi_{min},\xi_{max}]$. Another advantage is to have more balanced basis coefficients in terms of their absolute values because deviations from the equilibrium distribution are larger close the origin where $\xi$ is small.

We test weighted and unweighted B-splines as well as the weighted FCS as ansatz function (unweighted FCS do not make sense as the compatibility constraints already make use of the weight function).
Finally, the coefficients are found by means of a Galerkin method. The expansion is multiplied with test functions followed by integrating over velocity space and then solving the resulting system of equations.

We investigate the approximation properties using two bimodal functions
\begin{equation}
    f_{b1}(\xi)=0.527399 e^{-5.55556 (0.566569 \xi -0.3)^2}+0.169521 e^{-3.125 (0.566569 \xi +0.7)^2},
\end{equation}
\begin{equation}
    f_{b2}(\xi)=0.274485 e^{-3.125 (1.10085 \xi -0.525)^2}+0.274485 e^{-0.347222 (1.10085 \xi +0.175)^2}
\end{equation}
and the bimodal function used by Kauf in \cite{Kauf2011}
\begin{equation}
    f_{PK}(\xi)=0.16241 e^{-0.45116 (\xi -0.8)^2}+0.812051 e^{-6.34444 (\xi +0.6)^2}.
\end{equation}
The example functions were chosen such that they represent different shapes of bimodal functions frequently occurring in numerical simulations, for example, in the shock structure test case in Section \ref{sec:Shock_structure}.

Note that all distribution functions are chosen to have $\rho=1$, $v=0$ and $\theta=1$, to comply with the constraints in Equation \eqref{compatibility} in the transformed space, see Equation \eqref{trans} and Section \ref{sec:5}. All bimodal functions are plotted in Figure \ref{bimods}.
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.4]{maboGraphics/bimods}
 \caption{{Bimodal test functions} $f_{PK}$, $f_{b1}$, $f_{b2}$.}
 \label{bimods}
\end{figure}

\subsection{Unweighted B-splines}
\label{vorgehen}
Unweighted B-splines expand the deviation of the distribution function from the equilibrium Gaussian in a standard spline series using the following definition.
\begin{definition}[unweighted B-spline expansion]
    Let $k$ be the order and $G_{\xi}$ be a grid
    \begin{equation}
        G_{\xi}: \xi_1 = \xi_{min}-\frac{\Delta \xi(k+1)}{2} < \xi_2 = \xi_1+\Delta \xi < \hdots < \xi_{\#splines+k+1}=\xi_{max}+\frac{\Delta \xi(k+1)}{2},
    \end{equation}
    using a $\xi$-range $[\xi_{min},\xi_{max}]$ and equidistant grid spacing $\Delta \xi=\frac{\xi_{max}-\xi_{min}}{\#splines-1}$.

    The \emph{unweighted B-spline} expansion of a distribution function $f: \reals \mapsto \reals$ is then given by
    \begin{equation}
        \label{ansatz}
        \hat{f}(\xi)=\maboxinormal+\sum_{i=1}^{\#splines} \alpha_i \phi_i(\xi).
    \end{equation}
    for B-splines $\phi_i = B_{ki}$ of order $k$.
\end{definition}

%{%
%Unweighted B-splines expand the deviation of the distribution function $f: \reals \mapsto \reals$ from the equilibrium Gaussian $f_{normal}(\xi)=\maboxinormal$ in a standard Spline series. The ansatz reads
%}
%\begin{equation}
%\label{ansatz}
%\hat{f}(\xi)=\maboxinormal+\sum_{i=1}^n \alpha_i \phi_i(\xi).
%\end{equation}
%{%
%As ansatz functions $\phi_i$ we use B-splines $B_{ki}$ of order $k$. For the grid we fix a $\xi$-range $[\xi_{min},\xi_{max}]$. The equidistant grid uses the spacing $\Delta \xi=\frac{(\xi_{max}-\xi_{min})}{(n-1)}$. The grid is given by
%}
%\begin{equation}
%    G_{\xi}: \xi_1 = \xi_{min}-\frac{\Delta \xi(k+1)}{2} < \xi_2 = \xi_1+\Delta \xi < \hdots < \xi_{n+k+1}=\xi_{max}+\frac{\Delta \xi(k+1)}{2}.
%\end{equation}
%{%

Constructing the B-spline according to Section \ref{sec:3}, we obtain exactly $\#splines$ B-splines for an unweighted B-spline basis, where the outer B-splines' midpoints coincide with $\xi_{min}$ and $\xi_{max}$ (see Figure \ref{bereich}).

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.55]{maboGraphics/bereich}
 \caption{Example velocity grid with $7$ third-order splines. $[\xi_{min},\xi_{max}]=[-2,2]$.}%, i.e. $\Delta \xi=2/3$, $\xi_1=-10/3$, $\xi_{10}=10/3$.}
 \label{bereich}
\end{figure}

The basis coefficients are calculated by Galerkin projection of \ref{ansatz} onto test functions $\psi_j(x)=\phi_j$, $j \in 1,..,\#splines$. This leads to the linear system of equations
\begin{equation}
    \label{lgs}
    A \alpha = u,
\end{equation}
with solution $\alpha \in \mathbb{R}^{\#splines}$, while the entries $u \in \mathbb{R}^{\#splines}$ and $A \in \mathbb{R}^{\#splines \times \#splines}$ are given by
\begin{equation}
    u_j =  \inftyint f(x) \psi_j dx, \quad A_{ij}=\inftyint \phi_i(x) \psi_j(x) dx. \label{ansatzend}
\end{equation}

The unweighted B-spline ansatz was tested in a numerical study for the three bimodal distribution functions $f_{PK}$, $f_{b1}$ and $f_{b2}$ for different numbers of splines $n$ and various spline orders $k$ for constant $\xi$-range $\xi \in [\xi_{min},\xi_{max}]=[-4,4]$ with results shown in Figure \ref{approxUnweightedBPK}.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/pk1}
         \caption{$9$ splines.}
         \label{unweightedb/pk1}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/pk2}
         \caption{$17$ splines.}
         \label{unweightedb/pk2}
     \end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/pk3}
         \caption{$33$ splines.}
         \label{unweightedb/pk3}
     \end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/pk4}
         \caption{Zoom into (c). $33$ splines.}
         \label{unweightedb/pk4}
\end{subfigure}
\caption{Approximation of $f_{PK}$ with unweighted B-splines of different orders $k$, $[\xi_{min},\xi_{max}]=[-4,4]$.}
\label{approxUnweightedBPK}
\end{figure}
The approximation error decreases with the number of splines. We can also observe that B-splines of higher order are superior in approximation, although this has less effect than the number of splines.
Figure \ref{unweightedb/pk4}, which is an enlarged view of Figure \ref{unweightedb/pk3}, shows that splines of higher order fit the curves of the original function better and are thus more precise.
%We note that ansatz \eqref{ansatz} represents a spline expansion plus a Gaussian distribution, which is not a spline function itself and thus does not consist of pieces of polynomials.

The relation between the number of splines and the error $(\Delta f):=\|f-\hat f\|_{L2}$ for the three orders $k=1,2,3$ is displayed in Figure \ref{unweightedb/plot}. The error curves describe the arithmetically averaged relative error of all three distribution functions. The plot also shows lines with convergence rates $1,2,3$, corresponding to the (empirical) rate of convergence following the approximations with the respective orders $k=1,2,3$. Note that from $50$ splines onwards there is no further improvement for the splines of third order. This can be attributed to the fact that the splines have finite support, whereas the bimodal function's support is the whole $\xi$-axis. A small error of $10^{-4}$ thus remains. In order to clearly demonstrate this effect we consider the error on an even smaller $\xi$-range $[\xi_{min},\xi_{max}]=[-2,2]$ in Figure \ref{unweightedb/plot_smol}. Interestingly, a reduced $\xi$-range leads to better approximation results for a small number of spline functions. This is because for an unchanged number of splines, the B-splines now lie much denser ($\Delta \xi$ has decreased) and approximate the function better on that small domain around the origin. However, the limit is reached very soon and an error of $0.025$ remains. From $20$ splines onwards the approximation on the range $[-4,4]$ is clearly superior and reaches lower error values.
We will investigate this effect further when comparing with other ansatzes.

%Assuming that the error $(\Delta f)_n:=\|f-f_n\|_{L2}$ decreases exponentially with $n$, in other words, there exist $q \in \reals$ and $c \in \reals$ such that
%}
%\begin{equation}
%\label{DefQ}
%  (\Delta f)_n = c \left(\frac{1}{n}\right)^q,
%\end{equation}
%{%
%we call $q$ the rate of convergence.
%It was empirically determined for every two successive pairs $\{n_1,(\Delta f)_{n_1}\}$, $\{n_2,(\Delta f)_{n_2}\}$ by inserting them into Equation \ref{DefQ}.
%}

\begin{figure}[ht]
\centering
\begin{subfigure}[b]{0.48\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/plot_w_reference_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-4,4]$.}
         \label{unweightedb/plot}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.48\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/unweightedb/plot_smol_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-2,2]$.}
         \label{unweightedb/plot_smol}
\end{subfigure}
\caption{Approximation error depending on number of splines for unweighted B-splines and two different $\xi$-ranges.}
 \label{unweightedb}
\end{figure}


\subsection{{Weighted B-Splines}}
Weighted B-splines are multiplied with the Gaussian distribution and allow for a smoother distribution function with more weight around the center.
\begin{definition}[weighted B-spline expansion]
    Let $k$ be the order and $G_{\xi}$ be a standard grid using a $\xi$-range $[\xi_{min},\xi_{max}]$ and equidistant grid spacing $\Delta \xi=\frac{(\xi_{max}-\xi_{min})}{(\#splines-1)}$.

    The \emph{weighted B-spline} expansion of a distribution function $f: \reals \mapsto \reals$ is then given by
    \begin{equation}
        \hat{f}(\xi)=\maboxinormal \left( 1+ \sum_{i=1}^{\#splines} \alpha_i \phi_i(\xi)\right).
    \end{equation}
    for B-splines $\phi_i = B_{ki}$ of order $k$.
\end{definition}

The approximation of the three bimodal functions $f_{PK}$, $f_{b1}$ and $f_{b2}$ can then be computed using Galerkin projection with B-spline test functions $\psi_j=B_{jk}$. The coefficient vector $\alpha$ is determined according to the method presented in Subsection \ref{vorgehen}.

Figure \ref{approxWeighted} shows that there are fewer oscillations close to the boundary of the grid in comparison to the unweighted ansatz (compare for example Figure \ref{weightedb/pk1} and Figure \ref{unweightedb/pk1}). Apart from that the approximation looks equally accurate. The error reduction and the behavior for a smaller grid size in Figure \ref{weightedb} are the same as for the unweighted ansatz.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedb/pk1}
         \caption{$9$ splines.}
         \label{weightedb/pk1}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedb/pk2}
         \caption{$17$ splines.}
         \label{ightedb/pk2}
     \end{subfigure}
\caption{Approximation of $f_{PK}$ with weighted B-splines of different orders $k$, $[\xi_{min},\xi_{max}]=[-4,4]$.}
\label{approxWeighted}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.46\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedb/plot_w_reference_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-4,4]$.}
         \label{weightedb/plot}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.46\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedb/plot_smol_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-2,2]$.}
         \label{weightedb/plot_smol}
\end{subfigure}
       \caption{Approximation error depending on number of splines for weighted B-splines and two different $\xi$-ranges.}
 \label{weightedb}
\end{figure}


\subsection{{Weighted Fundamental Constrained Splines}}
Weighted FCS are used to conserve mass, momentum, and energy of the distribution function by using the previously developed FCS basis function plus Gaussian weight.
\begin{definition}[weighted FCS expansion]
    Let $k$ be the order and $G_{\xi}$ be a standard grid using a $\xi$-range $[\xi_{min},\xi_{max}]$ and equidistant grid spacing $\Delta \xi=\frac{(\xi_{max}-\xi_{min})}{(\#splines-1)}$.

    The \emph{weighted Fundamental Constrained Splines} expansion of a distribution function $f: \reals \mapsto \reals$ is then given by
    \begin{equation}
        \hat{f}(\xi)=\maboxinormal \left( 1+ \sum_{i=1}^{\#splines-3} \alpha_i \phi_i(\xi)\right)
    \end{equation}
    for fundamental constrained splines $\phi_i$ of order $k$.
\end{definition}

\begin{remark}
    By construction of the fundamental constrained splines, the following compatibility conditions are exactly fulfilled
    \begin{equation}
    \label{compat}
     \inftyint \hat{f}(\xi)dx=1,\quad
     \inftyint \xi \hat{f}(\xi)dx=0, \quad \textrm{and} \quad
     \inftyint \xi^2\hat{f}(\xi)=1,
    \end{equation}
    which equally hold for the bimodal functions $f_{PK}$, $f_{b1}$ and $f_{b2}$.
\end{remark}

Figure \ref{approxWeightedFCS} again shows fewer oscillations around the boundaries of the grid due to the weighting. A similar decrease of the approximation error with increasing number of splines is seen. There is some residual error due to the finite $\xi$-range as shown in Figure \ref{weightedfcs/plot}. We note that when extending the limits to $[\xi_{min}=-6,\xi_{max}=6]$, the error saturation happens at much lower error values, at around $10^{-6}$ in contrast to the saturation at $10^{-4}$ that we observe on the range $[-4,4]$. This result is not pictured for conciseness.

\begin{figure}[htb!]
\centering
\begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedfcs/pk1}
         \caption{$9$ splines.}
         \label{weightedfcs/pk1}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedfcs/pk2}
         \caption{$17$ splines.}
         \label{weightedfcs/pk2}
     \end{subfigure}
\hfill
\caption{Approximation of $f_{PK}$ with weighted FCS of different orders $k$, $[\xi_{min},\xi_{max}]=[-4,4]$.}
\label{approxWeightedFCS}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedfcs/plot_w_reference_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-4,4]$.}
         \label{weightedfcs/plot}
\end{subfigure}
\hfill\begin{subfigure}[b]{0.47\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/weightedfcs/plot_smol_new_legend}
         \caption{$[\xi_{min},\xi_{max}]=[-2,2]$.}
         \label{weightedfcs/plot_smol}
\end{subfigure}
       \caption{Approximation error depending on number of splines for weighted FCS and two different $\xi$-ranges.}
 \label{weightedfcs}
\end{figure}


\subsection{Approximation property}
The approximation properties of B-splines for standard continuous functions are well-known and can be found in the literature, for example, in \cite{bookPGS}, Chapter XII. Therein, the following theorem for the approximation error can be found, which reads for an equidistant grid:

\begin{theorem}[Spline approximation error]
    \label{th:Jackson Type}
    Let $\$_{k,t}$ denote the space of all B-splines of order $k$ on the equidistant grid $G: \, a=t_0 < t_1 < t_2 < \hdots < t_n = b$ with mesh size $|t|$ and $\| \cdot \|_{max}:=\max_{a\leq x \leq b}|g(x)|$.
    Then for each order $k$ there exists a constant $C_{k}$ so that for all smooth functions $g \in C^{(k+1)}[a\hdots b]$
    $$dist(g,\$_{k,t}) \coloneqq min\{\| g - s \|_{max} : s \in \$_{k,t}\} \leq C_k |t|^{k+1} \|g^{(k+1)}\|_{max}\;.$$
\end{theorem}

Theorem \ref{th:Jackson Type} states that the approximation error decreases at least with the $k$th power of the mesh size, corresponding to our observations regarding the convergence speed, where we observed the respective higher rates of convergence for splines of higher order. With the additional restriction that the function $g$ from \ref{th:Jackson Type} fulfills the compatibility conditions in Equation \ref{compat}, we can formulate the same theorem for the weighted FCS.


\subsection{{Spline Approximation Convergence}}
In Figure \ref{cmpConvergence} the respective error evolution of each ansatz is compared using the standard support $[\xi_{min},\xi_{max}]=[-4,4]$. For the linear splines in Figure \ref{cmpLin}, weighted and unweighted B-splines as well as weighted FCS show the same convergence behavior with an initially slightly smaller error for the weighted FCS model. For all models we observed that the error reaches a certain plateau. The plateaus for the linear case in Figure \ref{cmpLin} are not visible because they are reached at a larger spline number than 50. For a smaller support, the plateaus are reached earlier, resulting in a larger remaining error (not shown here for conciseness). We also observed that the remaining error for the FCS is slightly higher than the remaining error for the other models. The plateaus are in all cases a combination of the limited support of the spline basis and of rounding errors that occurred during the computation of the coefficients, particularly while solving the linear system \eqref{lgs} using an ill-conditioned matrix. Additional errors for the FCS can occur during the construction of the fundamental constrained splines, i.e. solving the linear system in Equation \ref{fcslgs}. However, in practice we do not expect to use more than $15-20$ splines to balance accuracy and efficiency. In this region, no problems with stability occur and the error remains very small in all tested cases.

\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpLin_w_reference_new_legend}
         \caption{$k=1$.}
         \label{cmpLin}
\end{subfigure}
\hfill
%\begin{subfigure}[b]{0.49\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpLin_smol_paper}
%         \caption{$k=1$, $[\xi_{min},\xi_{max}]=[-2,2]$.}
%         \label{cmpLin_smol}
%\end{subfigure}
%\hfill
\hfill
\begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpQuad_w_reference_new_legend}
         \caption{$k=2$.}
         \label{cmpQuad}
\end{subfigure}
\hfill
%\begin{subfigure}[b]{0.49\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpQuad_smol_paper}
%         \caption{$k=2$, $[\xi_{min},\xi_{max}]=[-2,2]$.}
%         \label{cmpQuad_smol}
%\end{subfigure}
%\hfill
\hfill
\begin{subfigure}[b]{0.49\textwidth}
         \centering
         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpCubic_w_reference_new_legend}
         \caption{$k=3$.}
         \label{cmpCubic}
\end{subfigure}
\hfill
%\begin{subfigure}[b]{0.49\textwidth}
%         \centering
%         \includegraphics[width=\textwidth]{maboGraphics/approx/cmpCubic_smol_paper}
%         \caption{$k=3$, $[\xi_{min},\xi_{max}]=[-2,2]$.}
%         \label{cmpCubic_smol}
%\end{subfigure}
%\hfill
\caption{Approximation error depending on number of splines for different orders and basis functions, $[\xi_{min},\xi_{max}]=[-4,4]$.}
 \label{cmpConvergence}
\end{figure}

In summary, we observe that the approximation of bimodal distribution functions via weighted FCS yields the best accuracy while conserving mass, momentum, and energy of the distribution function. We will thus also use the weighted FCS basis for the derivation of a set of PDEs for the dynamic evolution of the coefficients in the following section.

\begin{remark}
    In this paper, we focus on the one-dimensional Boltzmann-BGK equation. There exist several possibilities to extend the spline basis to a multi-dimensional setting. In principle, a tensorized ansatz with or without certain adaptivity for different directions as outlined in \cite{Koellermeier2018,Torrilhon2015} can be used. According to the tests in this section, a bounded support for the spline basis yields sufficient accuracy in a 1D setup, so that the full multi-dimensional basis might not need to include too many basis functions. This is especially true as we use the spline basis for the transformed Boltzmann-BGK equation introduced in Section \ref{sec:5}. We note that this is a crucial difference to existing models based on DG discretizations in non-adaptive velocity spaces, see e.g. \cite{Zhang2018,Alekseenko2012,Alekseenko2014}. In our case, the resulting moment model will be highly non-linear, but efficient and accurate at the same time.
\end{remark}
