% vim: wrap:tw=100:spell:colorcolumn=101
\documentclass[11pt]{article}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{amssymb, amsmath, color}
\usepackage{esint}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subfig}

\newcounter{reviewer}
\newcounter{question}
\newcommand\startResponse{\noindent\stepcounter{reviewer}%
    \paragraph{Reply to Reviewer \thereviewer:}~\\}
\newcommand\question[1]{\noindent\stepcounter{question}{\bf Comment \thequestion}: {#1}}
\newcommand\answer[1]{\vspace*{1mm} \par \noindent{\bf Answer \thequestion}: {#1}\vspace*{0.5cm}}
\setcounter{question}{0}

\newcommand\fy[1]{{\color{red}#1}}
\newcommand\ly[1]{{\color{green}#1}}
\newcommand\todo[1]{{\color{red}To do: #1}}
\newcommand\comment[1]{{\color{red}(#1)}}

\newcommand\cH{{\mathcal{H}}}

\begin{document}
\title{Reply to the reviewers' reports for the paper\\
Spline Moment Models for the one-dimensional Boltzmann-Bhatnagar-Gross-Krook equation
}
\author{Julian Koellermeier and Ullika Scholz}

\maketitle

We would like to thank the reviewer sincerely for the careful reading of our manuscript and the valuable comments that have improved the quality of our manuscript substantially. We briefly refer to the main changes first, please see the detailed answers to the reviewers' questions for additional changes with respect to the first version of the manuscript.

The main changes are
\begin{itemize}
  \item[1.] Addition of section "4.6 Approximation of a discontinuous distribution function".
  \item[2.] Addition of section "6.1.4 Computational efficiency".
  \item[3.] Addition of supplementary references, removing of spelling errors and inconsistencies.
\end{itemize}

Below we present our more detailed answers to the reviewer's questions regarding our modifications to the manuscript. Note that all specific changes can be found in the diff-version of the manuscript.

\vspace*{3mm}

\paragraph{Reply to associate editor :}~\\

\question{To strengthen your case for novelty and relevance, consider more recent fluids papers (last 5 years).}
\answer{We have added several recent references to position the current work better within the ongoing research. }


\paragraph{Reply to reviewer 1:}~\\
%\renewcommand{\labelitemi}{$\ast$}

The authors proposed a new spline moment equations for BGK kinetic model equation, using weighted constrained spline functions to approximate the velocity distribution in the molecular velocity space. The weighted constrained spline functions allow discretization with adaptive grid and conservations of mass, momentum and energy. The method is new and the paper is appropriate for the special issue in Physics of Fluids. However, I have some questions and suggestions to modify the paper and to make it more clear.\\

\question{Line 57, please add the full name before the abbreviations for HME and QBME.}
\answer{We have included the full versions of the abbreviations (HME = Hyperbolic Moment Equations, QBME = Quadrature-Based Moment Equations) upon first occurrence in the text.}

\question{One constrained spline function is constructed using 4 weighted B-splines for 1D system. Does the number of splines increase for higher-dimension system?}
\answer{If the microscopic velocity domain is one-dimensional, there are always 4 weighted B-splines that form a Fundamental Constrained Spline. However, in the multi-dimensional setting, there are more constraints (in general there are $d+2$ constraints), so that more splines are necessary. We have included a remark to point that out.

"In the 1D setting discussed here, the three constraints (3.8) lead to the reduction of the dimension of the function space by three. However, in a multi-dimensional setting, e.g. with a multi-dimensional spline basis obtained from the tensor product of one-dimensional splines, there will be more constraints and thus more splines that build a Fundamental Constrained Spline. In a general, $d-$dimensional setting, there are $d+2$ constraints."
}

\question{Please check Eq. (3.9).}
\answer{The wrong indices have been corrected.}

\question{The authors state "the distribution function will be scaled and shifted in the velocity variable to a new velocity space". However, the velocity space is not new. They just scale and shift the distribution function. The velocity space is still $[-\infty, \infty]$.}
\answer{The transformation in the velocity variables indeed only scales and shifts the distribution function, whereas the velocity space is still the whole space. We clarified that and changed the sentence to

    "...the distribution function $f(t,x,c)$ is scaled and shifted in the velocity variable to a new distribution function $f(t,x,\xi)$..."
}

\question{Line 257, is "$\sharp$splines" means the number of splines? Is it the one denoted as "n" later? Please clarify this and keep consistent over the paper.}
\answer{The notation of "$\sharp$splines" was changed and only the notation with "$n$" is now used consistently throughout the paper.}

\question{In section 4, the authors test the performance of three different basis functions to approximate the velocity distribution functions. However, the velocity distribution functions are generally smooth, which are usually encountered in the circumstance of shock wave. How are their performance to approximate velocity distribution functions that have discontinuities, as the ones usually appear in low-speed nano/macro flows near the boundaries with sharp corners?}
\answer{We have included a new section 4.6 with a discontinuous example distribution function that is commonly encountered in boundary value problems where the temperature of the wall is different from the temperature of the fluid. We test the weighted fundamental constrained spline approximation for this distribution function and obtained good approximation quality of the new spline model for increasing number of splines. See the new section 4.6 for more details.}

\question{In Figure 15, what's 'sol' and what is the Knudsen number? Please add the information in the caption.}
\answer{We have added the Knudsen number $Kn=0.5$ and the usage of "sol" for the reference solution from a DVM simulation in the caption of the respective Figure.}

\question{It seems that the SME works well for small Knudsen number, however, noticeable discrepancies between the results of SME and reference data can be observed when Knudsen number increases. I suggest to add convergence test with respect to the Knudsen number, in order to show the number and order of splines required for accurate solutions in different flow regimes.}
\answer{We have added a new section "6.1.4 Computational efficiency", in which we compare the results of the new spline model to reference solutions for different Knudsen numbers. It can clearly be seen that the spline model converges (up to a small remaining numerical error) towards the reference solution when increasing the number of splines and when decreasing the Knudsen number. See also the reply to the next comment.}

\question{Please add the computational time to show the balance between accuracy and efficiency.}
\answer{In the new section "6.1.4 Computational efficiency", we have included the computational times of the spline models with different number of splines $n$. We can see that the more accurate solutions obtained with larger $n$ obviously require more runtime, but also result in a smaller error. This behavior is the same for all Knudsen numbers.}


%\paragraph{Reply to reviewer 2:}~\\
%
%This paper investigates the approximation property of the splines, such as unweighted splines, weighted splines and weighted fundamental constrained splines (FCS), for the distribution function. The spline moment equation (SME) for one-dimensional Boltzmann-BGK equation is derived using the FCS which could conserves mass, momentum and energy of the approximated distribution function. The numerical results show that the new SME model could provide accurate solutions with decreasing error when using more equations, and gain higher efficiency than discrete velocity method.
%
%I recommend the publication of the paper in Physics of Fluids after the following minor issues have been addressed.\\
%
%\question{The introduction about the discrete velocity method should be revised (line 42-50). The UGKS is one of DVMs due to the utilization of discrete velocities points, it may be misleading to introduce the UGKS together with the macroscopic methods extended from the Navier-Stokes equations.}
%\answer{We have included this useful advice and moved the reference of UGKS to the end of the paragraph about DVMs as follows
%
%"A method that is also based on the use of discrete velocity points is the Unified Gas Kinetic Scheme (UGKS) \cite{Xu2010}."
%}
%
%\question{In the numerical computations, very fine spatial grids with thousands of points are usually employed. Is the current method sensitive to the spatial discretization? If possible, comparison and study of the numerical solutions on coarse grids for one test case are preferred.}
%\answer{As the splines are used for the discretization of the microscopic variable $c$ (or its shifted and scaled counterpart $\xi$) only, there is no need to assume that the method is sensitive to the spatial discretization in $x$. In fact, there have been numerical tests with similar models based on Hermite polynomials that showed that the method is insensitive to the spatial discretization \cite{Koellermeier2017}. In this paper, we are interested in the model accuracy for a discretization in microscopic velocity space based on splines. We thus choose a fine spatial discretization similar to the tests in \cite{Cai2013, Koellermeier2017b, Koellermeier2017, schaerer2015}. We have added the following clarification to the simulation results section to explain the choice of the fine spatial grids:
%
%"In order to assess the model accuracy and not have the accuracy spoilt by numerical errors resulting from the spatial discretization, we choose a rather fine spatial discretization similar to the tests in \cite{Cai2013, Koellermeier2017b, Koellermeier2017, schaerer2015}. However, it is clear that the model can also be used for simulations with coarser spatial grids, which will only affect the spatial discretization error and not the model accuracy itself."
%}
%
%\question{Several typos should be amended in revised manuscript, such as the subscripts in Eq.(3.9), and "boltzmann" and "galerkin" in Reference.}
%\answer{The manuscript has been proofread again, the mistakes in the subscripts have been corrected, and references have been revised, including correct spelling of the terms "Boltzmann" and "Galerkin".}

\paragraph{Reply to the editorial office:}~\\

\question{Revised manuscript source files must be in Word or LaTeX/ReVTeX. Replace the current PDF file with an acceptable manuscript source file.}
\answer{We have provided a single ".tex" file in place of the previous compiled PDF file.}

\question{Separate figure files (separate from text and tables) are required for production purposes. Submit a separate figure file for each figure cited in the manuscript, not just one file containing all figures. The allowed file formats for figure files are TIF, PS, EPS, JPEG, or high-quality PDF. Figures may remain embedded in the manuscript. Number your separate figure files as they appear in the manuscript. For example 1, 2, 3.
NOTE: DOC or DOCX figure files are not permitted.}
\answer{We have provided the figure source files in an appropriate format. The files have consecutive naming.}
%\todo{US: can you rename all the figure files according to the number they appear in the text? (for the figure files themselves and also in the .tex} This should be done in the end since we want to add more figures}

\question{Figures 3, 6-13, 15-25 contain multiple figure parts. Add these sublabels to your captions.}
\answer{The figure captions have been changed and now include information about the subfigures.}
%\todo{US: I think they mean that we should write name the figure files included in Figure 3 as 3a, 3b,... Can you do that?}}

\question{The manuscript begins with a title page which includes names of all authors and their affiliations, and an abstract, followed by the body of the paper, Conclusions, Supplemental Material, Acknowledgment, Appendix, Data Availability section, and References..}
\answer{We have adjusted the order of the sections such that the appendix now precedes the references and included an acknowledgement and data availability section at the appropriate places.}


%\bibliographystyle{abbrv}
%\bibliography{../spline_moment_equations_paper}

\end{document}
