\section{Spline Basis Functions}
\label{sec:3}
Many approximations exist to derive accurate discretizations of the BTE in velocity space. However, up to our knowledge there is no work investigating the natural use of spline ansatz functions, although splines are used in many applications in engineering with great success. In this paper, we will therefore use the flexible spline ansatz for the distribution function. We parameterize the distribution function by means of ansatz functions multiplied by respective coefficients. In this section we will introduce the ansatz functions that compose the basis space. The two classes of possible ansatz functions presented here are B-splines and Fundamental Constrained Splines (FCS).

\subsection{B-Splines}
\label{bsplineexamples}
B-splines were first presented as \emph{fundamental spline functions} to serve as a basis for the known spline functions \cite{paperSC}. The class of spline functions is very popular and for example used in polynomial interpolation. Its members are piecewise functions $S: \reals \mapsto \reals$ consisting of polynomials of equal degree.
The pieces are defined by a grid $G_{\xi}=[\xi_0, \hdots, \xi_{n-1}]$ and the polynomial degree is called the spline's \emph{order} $k \in \naturals$. A spline of order $k$ is at least $k-1$ times continuously differentiable by construction.

Because of its simplicity we will here use the recursive definition for B-splines \cite{bookPGS}. The initial definition is not recursive and can be found in \cite{paperSC}.

\begin{definition}[B-spline]
    Let $G_{\xi}: \hdots < \xi_{-2} < \xi_{-1} < \xi_{0} < \xi_{1} <  \xi_{2} < \hdots  ,\: \: \xi_i \in \reals$ be a grid.

    The $j$th B-spline of order $k=1$ is defined by
    \begin{equation}
    \label{BSplineOder1Def}
    B_{j1}(\xi) :=
    \begin{cases}
     \frac{\xi-\xi_j}{\xi_{j+1}-\xi_{j}} & \xi \in [\xi_j,\xi_{j+1}] \\
     \frac{-\xi+\xi_{j+2}}{\xi_{j+2}-\xi{j+1}} & \xi \in [\xi_{j+1},\xi_{j+2}] \\
     0 & \textrm{else}.
    \end{cases}.
    \end{equation}
    B-splines of higher order are obtained by the recursion formula
    \begin{equation}
    \label{SplineDef}
     B_{jk}(\xi)=\omega_{jk}^{(1)}B_{j,k-1}(\xi)+\omega_{jk}^{(2)} B_{j+1,k-1}(\xi),
    \end{equation}
    with
    \begin{equation}
     \omega_{jk}^{(1)}(\xi):=\frac{\xi-\xi_j}{\xi_{j+k}-\xi_j} \quad \textrm{and} \quad \omega_{jk}^{(2)}(\xi):=\frac{-\xi+\xi_{j+k+1}}{\xi_{j+k+1}-\xi_{j+1}}.
    \end{equation}
\end{definition}

\begin{remark}
    The recursion formula implies that a B-spline of order $k$ is a composition of two neighboring B-splines of order $k-1$. The B-splines' support therefore increases by one grid cell per order, that is
    $\supp B_{jk}(\xi)=[\xi_j,\xi_{j+k+1}]$.
    A proof can be found in \cite{bookPGS} p. 91. On an equidistant grid of width $\Delta \xi$ this simplifies to $\supp B_{jk}(\xi)=[\xi_j,\xi_{j}+(k+1)\Delta \xi]$.
\end{remark}

We exemplify B-splines of first and second order. For simplicity we choose the grid $G_{\xi}$ in such a way that the first B-spline $B_{1k}$ is centered around the origin. Order $k=1$ then results in the grid
\begin{equation}
 G^{(1)}_{\xi} : \hdots < \xi_0 = -2\;\Delta \xi < \xi_1 = -\Delta \xi < \xi_2 = 0 < \xi_3 = \Delta \xi <  \hdots \: .
\end{equation}
The first B-spline of first order on $G^{(1)}_{\xi}$ is
\begin{equation}
B_{1,1}(\xi)=
\begin{cases}
 1-\frac{\xi }{\Delta \xi } & 0\leq \xi \leq \Delta \xi  \\
 \frac{\xi }{\Delta \xi }+1 & -\Delta \xi \leq \xi \leq 0 \\
 0 & |\xi|>\Delta \xi.
\end{cases}
\end{equation}
Figure \ref{border1} contains this B-spline $B_{1,1}$ together with its neighbors $B_{2,1}$ and $B_{3,1}$ on the grid $G^{(1)}_{\xi}$ for $\Delta \xi=1$. These first order B-splines are often denoted as \emph{hat functions}.
\begin{figure}[H]
 \centering
 \includegraphics[scale=0.3]{maboGraphics/border1}
 \caption{Linear splines on $G^{(1)}_{\xi}$, $\Delta \xi=1$.}
 \label{border1}
\end{figure}

Splines of second order $k=2$ are piecewise quadratic polynomials. In comparison with \ref{border1} a different grid $G^{(2)}_{\xi}$ is used

\begin{equation}
G^{(2)}_{\xi} : \hdots < \xi_0 = -\frac{5}{2}\Delta \xi\; < \xi_1 = -\frac{3}{2}\Delta \xi < \xi_2 = -\frac{1}{2}\Delta \xi < \xi_2 = \frac{1}{2} \Delta \xi < \hdots \: .
\end{equation}

On this grid the first B-spline of second order is

\begin{equation}
B_{2,1}(\xi)=
\begin{cases}
 \frac{1}{8} \left(\frac{2 \xi }{\Delta \xi }-3\right)^2 & \frac{\Delta \xi }{2}\leq \xi \leq \frac{3 \Delta \xi }{2} \\
 \frac{3}{4}-\frac{\xi ^2}{\Delta \xi ^2} & -\frac{\Delta \xi }{2}\leq \xi \leq \frac{\Delta \xi }{2} \\
 \frac{1}{8} \left(\frac{2 \xi }{\Delta \xi }+3\right)^2 & -\frac{3 \Delta \xi }{2}\leq \xi \leq -\frac{\Delta \xi }{2}\\
 0 & |\xi|>\frac{3}{2} \Delta \xi.
\end{cases}
\end{equation}

This B-spline is depicted in Figure \ref{border2} along with its neighbors.

\begin{figure}[H]
 \centering
 \includegraphics[scale=0.3]{maboGraphics/border2}
 \caption{Quadratic splines on $G^{(2)}_{\xi}$, $\Delta \xi=1$.}
 \label{border2}
\end{figure}

While B-splines are legitimate basis for piecewise polynomials, they do not conserve mass, momentum, and energy of the distribution function when used in an expansion in velocity space.


\subsection{Fundamental Constrained Splines}
Constrained splines are a subset of the spline functions. They will be used to discretize the velocity space for the Boltzmann equation because they preserve the distribution function's mass, momentum, and energy, which are related to the integral values $\rho$, $v$ and $\theta$, see Equation \eqref{makro}. In the derivations in Section \ref{sec:5} the distribution function is scaled and shifted in the velocity variable to a new velocity space $\xi$ so that the scaled distribution function has $\rho=1$, $v=0$ and $\theta=1$. Any constrained spline approximation function $S^{constr}$ then needs to fulfill the constraints
\begin{equation}
    \int_{-\infty}^{\infty} \maboxinormal S^{constr}(\xi) \begin{pmatrix}1\\\xi\\\xi^2\end{pmatrix} d\xi=\begin{pmatrix}0\\0\\0\end{pmatrix}.
    \label{compatibility}
\end{equation}
Any basis function for these constrained splines is called \emph{Fundamental Constrained Spline} (FCS) $F^{constr}$. Their construction from an existing B-spline basis is described in \cite{Kauf2011} and will be presented below.

\begin{definition}[Fundamental Constrained Splines]
    Given a grid with four consecutive B-splines $B_{jk}, B_{j+1,k}, B_{j+2,k}, B_{j+3,k}$, a \emph{Fundamental Constrained Spline} (FCS) $F_i^{constr}$ is assembled by linear combination

    \begin{equation}
     F_j^{constr}=a_0 B_{jk}+a_1 B{j+1,k} + a_2 B{j+2,k} + a_3 B_{j+3,k} \textrm{{\;,}}
    \end{equation}

    where the coefficients $\alpha_0, \ldots, \alpha_3 \in \reals$ are determined by

    \begin{gather}
    \begin{split}
    a_0=\frac{\tilde{a_0}}{\sqrt{\tilde{a_0}^2+\tilde{a_1}^2+\tilde{a_2}^2+1}}\;,\quad
    a_1=\frac{\tilde{a_1}}{\sqrt{\tilde{a_0}^2+\tilde{a_1}^2+\tilde{a_2}^2+1}}\;,\\
    a_2=\frac{\tilde{a_2}}{\sqrt{\tilde{a_0}^2+\tilde{a_1}^2+\tilde{a_2}^2+1}}\;,\quad
    a_3=\frac{\tilde{a_3}}{\sqrt{\tilde{a_0}^2+\tilde{a_1}^2+\tilde{a_2}^2+1}}\;.
    \end{split}
    \end{gather}

    Setting $\tilde{a_3}=1$, the other variables $\tilde{a_0}$, $\tilde{a_1}$ and $\tilde{a_2}$ are found by solving the linear system

    \begin{equation}
    \label{fcslgs}
    \sum_{j=0}^2 \tilde{a_j} \int_{-\infty}^{\infty} \begin{pmatrix}1\\\xi\\\xi^2 \end{pmatrix} e^{(-\xi^2/2)} B_{\alpha+j,k}(\xi)d\xi=-\int_{-\infty}^{\infty} \begin{pmatrix}1\\\xi\\\xi^2 \end{pmatrix} e^{(-\xi^2/2)} B_{\alpha+3,k}(\xi)d\xi.
    \end{equation}
\end{definition}

\begin{remark}
    A consequence of the necessity of four B-splines for the construction of one single fundamental constrained spline is that the dimension of the function space decreases by three when changing from a B-spline basis to a basis of fundamental constrained splines.
\end{remark}

Figure \ref{forder1} shows a fundamental constrained spline of first order $F_{1,1}$. The underlying grid is $G^{(1)}_{\xi}$ from \Eqn \ref{bsplineexamples} with $\Delta \xi=1$. It can be seen that $F_{1,1}$ is a linear combination of the B-splines $B_{1,1}$, $B_{2,1}$, $B_{3,1}$ and $B_{4,1}$. The equivalent holds true for spline orders $k=2$ and $k=3$ as depicted in Figures \ref{forder2} and \ref{forder3}.

The full basis of fundamental constrained splines for this example with $13$ basis elements is pictured in Figure \ref{forder4}.
\begin{figure}[h]
 \centering
 \begin{subfigure}[t]{0.47\textwidth}
 \includegraphics[width=\textwidth]{maboGraphics/forder1}
 \caption{Linear element $F_{1,1}$.}
 \label{forder1}
 \end{subfigure}
 \hfill
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/forder2}
 \caption{Quadratic element $F_{1,2}$.}
 \label{forder2}
 \end{subfigure}
\hfill
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/forder3}
 \caption{Cubic element $F_{1,3}$.}
 \label{forder3}
 \end{subfigure}
 \hfill
  \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/constrBasis}
 \caption{Complete linear basis, $13$ elements.}
 \label{forder4}
 \end{subfigure}
 \caption{Fundamental Contrained Spline basis for different order $k$, $\Delta \xi=1$.}
  \label{forders}
\end{figure}

\begin{remark}
  In contrast to the B-splines in Figure \ref{border1}, the FCS have a larger support and change their shape depending on their position on the $\xi$-axis. The FCS will later be multiplied with a local equilibrium weight function, which is the scaled Maxwellian \ref{e2:Maxwellian}, i.e. $\maboxinormalflat$. Values in the region of large $\xi$ will thus be scaled down accordingly.
\end{remark} 