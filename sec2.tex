\section{Boltzmann Transport Equation}
\label{sec:2}
According to \cite{bookRGD} the Boltzmann transport equation (BTE) is used to model fluid motions where the molecules' mean free path $\lambda$ is large in comparison to a reference length scale $L$ (for example, the diameter of a tube or the curvature radius of a space shuttle). The mean free path is the average distance that a molecule travels before it collides with another molecule. The dimensionless flow parameter, the Knudsen number, is denoted as $\textrm{Kn} = \frac{\lambda}{L}$, for details see e.g. \cite{Koellermeier2017b,Struchtrup2006}.
Typical applications for a large Knudsen number are rarefied gases and vacuum technology, as stated in \cite{bookRGD}. For small Knudsen numbers the BTE blends into the standard macroscopic conservation laws, i.e. the Navier-Stokes or Euler equations \cite{bookRGD}.

For conciseness, we only use the one-dimensional spatial case here and leave the extension to the multi-dimensional case for further work.

The BTE describes the dynamics of the distribution function $f$ which is a probability density defined at every position in space $x \in \reals$, time $t \in \reals_+$ and velocity space $c \in \reals$ \cite{bookRGD,Koellermeier2017b}.
\begin{equation}
 \dt f(x,t,c) + c \dx f(x,t,c) = S(f)
\end{equation}
The left-hand side describes the transport of particles, whereas the right-hand side contains the collision operator. Throughout the paper, we will use the BGK collision term $S(f)$ \cite{paperPGK}. In this model the distribution function $f$ relaxes towards the equilibrium function $f_{M}$ as a consequence of the collisions following
\begin{equation}
 S(f)=-\frac{1}{\tau}(f-f_{M}).
\end{equation}
Here $\tau$ is the relaxation time. Depending on the gas properties, it may be large, i.e. few collisions and slow relaxation towards $f_M$, or small, i.e. many collisions and fast relaxation of particle velocities towards equilibrium. The equilibrium limit $f_M$ is the Maxwellian distribution function, where the particle velocities are distributed according to a bell curve with its position and scaling determined by the macroscopic quantities density $\rho \in \reals$, temperature $\theta \in \reals$ and velocity $v \in \reals$.
\begin{equation}
\label{e2:Maxwellian}
 f_{M}=\frac{\rho(x,t)}{\sqrt{2\pi \theta(x,t)}} \exp{(-\xi^2 /2)}, \quad \xi = \frac{c-v(t,x)}{\sqrt{\theta(t,x)}}
\end{equation}
Density $\rho$, velocity $v$ and temperature $\theta$ are linked to the distribution function $f$ via integration in velocity space.
\begin{gather}
\label{makro}
 \begin{split}
 \rho(t,x) &= \inftyint f(t,x,c)dc \\
 \rho(t,x)v(t,x) &= \inftyint c f(t,x,c)dc \\
 \rho(t,x)\theta(t,x) &= \inftyint \vert c-v\vert^2f(t,x,c)dc.
\end{split}
\end{gather}
The Boltzmann-BGK equation thus forms a non-linear integro-differential equation. It is hard to solve the equation numerically because discretization is necessary in the microscopic velocity $c \in \reals$, the time $t \in \reals_+$ and the position in space $x \in \reals$.
