\section{Simulation Results}
\label{sec:6}

Three different test cases are used for the simulation of the new SME model. We will, however, focus on standard benchmark initial value problems and stationary problems, without the influence of boundary values. The definition of boundary values for moment models is a separate topic. An outline on how to derive boundary conditions can already be found in \cite{Grad1949} and applications can be found e.g. in \cite{Cai2018}. The derivation of boundary conditions for the SME model can in principle be done in the same way but is left for future work.


\subsection{Shock Tube Test Case}
We employ a standard shock tube test case for the first simulations of the new SME model \eqref{AsysDGL}. This test case is widely used with the same settings as in \cite{Cai2013,Koellermeier2017b}.
\begin{figure}[H]
 \centering
 \includegraphics[scale=.3]{maboGraphics/shockTube}
 \caption{Set-up shock tube test.}
 \label{shockTubeSketch}
\end{figure}

The initial density ratio is given by $\rho_L=7$ and $\rho_R=1$, while the gas is at a rest with uniform temperature of $\theta=1$. The pressure is computed using $p = \rho \theta$. The BGK collision operator uses a non-linear relaxation time $\tau = \textrm{Kn}/ \rho$ to model collisions.

The numerical simulation was conducted with the explicit first order path-consistent non-conservative finite volume solver from \cite{Koellermeier2017b} for time step size $\Delta t = 0.0001$, which corresponds to a CFL number of approximately $0.5$. The results are shown at $t_\textrm{END}=0.3$. We consider a small Knudsen number $\textrm{Kn}=0.05$ and a larger Knudsen number $\textrm{Kn}=0.5$ for more rarefied conditions. We will start from a base test setting of $n=4$ and $k=1$, $[\xi_{min},\xi_{max}]=[-2,2]$ and subsequently vary the different parameters. As a reference for the exact solution we use results from a high-resolution discrete velocity method also used in \cite{Koellermeier2017b}.


\subsubsection{Effect of the Spline Order $k$}
In Figure \ref{diforders} a test case with varying spline order is shown and only minimal differences can be observed. Only in the enlarged view slightly different subshock positions can be observed, whereas the main features remain unchanged.
\begin{figure}[ht]
 \centering
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/priforders2}
 \caption{Full view.}
 \label{priforders2}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.5\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/priforderszoomed}
 \caption{Enlarged view of (a).}
 \label{diforderszoomed}
 \end{subfigure}
\caption{Shock tube result for different spline order $k$ from bright to dark, $[\xi_{min},\xi_{max}]=[-2,2]$, $n=4$. Left axis for $\rho$ and $p$, right axis for $v$.}
\label{diforders}
\end{figure}
Most notably, we do not achieve better results for splines of higher order. A plausible explanation is the same effect that we have already seen when discussing the approximation properties in Figure \ref{unweightedb/plot}. When the number of splines is small in relation to the $\xi$-range, spline functions can only very roughly scan the original function's shape. Smaller details like rounded edges, that are the strength of higher-order splines, are not yet relevant here.

\begin{remark}
\label{remark_hyp}
    Figure \ref{diforders} clearly shows a subshock pattern, which is characteristic for moment models. The same can be observed for the standard Grad, HME or QBME models described in \cite{Koellermeier2017b}. The reason is the underlying set of bounded propagation speeds of the model. The subshocks typically become smaller when increasing $n$ and eventually vanish, as we will see later.
\end{remark}


\subsubsection{{Effect of the Support $[\xi_{min},\xi_{max}]$}}
To compare different $\xi$-ranges in a reasonable way, the number of splines should remain unchanged when extending the $\xi$-range. The spline grid parameter $\Delta \xi=\frac{\xi_{max}-\xi_{min}}{(n-1)}$ is therefore kept constant.

We show simulations on the three $\xi$-ranges $[\xi_{min},\xi_{max}]=[-2, 2]$, $[-3, 3]$ and $[-4, 4]$ for all three orders. For $[-5,5]$ and $[-6,6]$ the simulation in the case $\textrm{Kn}=0.5$, $k=1$ was unstable for some particular numbers of splines. This may be caused by the occurrence of imaginary eigenvalues. However, the simulation was successful for the spline orders $k=2$ and $k=3$ with otherwise the same parameter setting.

%We fixed a spline distance of $\Delta \xi=\frac{2}{3}$ to compare the simulation outcomes for the three smaller ranges in Figure \ref{resultsDifferentBases}.

Figure \ref{resultsDifferentBases} shows the results for a fixed spline distance of $\Delta \xi=\frac{2}{3}$.
It can be seen that the results are more accurate for a wider $\xi$-range. This effect is much stronger at the transition from $[-2,2]$ to $[-3,3]$ than at the transition from $[-3,3]$ to $[-4,4]$, probably due to the fact that the distribution function is decreasing for larger values of $\xi$. It is thus less problematic to assume $f(\xi)=0$ far away from the origin. For the largest range, the solution has already converged in the case $\textrm{Kn}=0.05$ and it is very accurate for $\textrm{Kn}=0.5$.
\begin{figure}[H]
 \centering
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base1}
 \caption{7 {splines on} $[-2,2]$, $\textrm{Kn}=0.05$.}
 \label{base1}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base2}
 \caption{7 {splines on} $[-2,2]$, $\textrm{Kn}=0.5$.}
 \label{base2}
\end{subfigure}
\hfill
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base3}
 \caption{10 {splines on} $[-3,3]$, $\textrm{Kn}=0.05$.}
 \label{base3}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base4}
 \caption{10 {splines on} $[-3,3]$, $\textrm{Kn}=0.5$.}
 \label{base4}
\end{subfigure}
\hfill
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base5}
 \caption{13 {splines on} $[-4,4]$, $\textrm{Kn}=0.05$.}
 \label{base5}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/base6}
 \caption{13 {splines on} $[-4,4]$, $\textrm{Kn}=0.5$.}
 \label{base6}
\end{subfigure}
\caption{Shock tube for different $\xi$-ranges with constant spline distance $\Delta \xi=\frac{2}{3}$.}
\label{resultsDifferentBases}
\end{figure}


\subsubsection{{Effect of the Number of Splines}}
In Figures \ref{kase} and \ref{resultsDifferentN} the number of splines is increased while keeping the $\xi$-range constant. For the smaller Knudsen number in Figure \ref{kase}, the solution is already very accurate for $4$ splines (corresponding to $n=7$ equations) and does not change much for larger numbers of splines or equations. For the larger Knudsen number in Figure \ref{resultsDifferentN} however, we can see the improvement clearly by observing smaller subshocks and a better accuracy in all regions of the domain.

%Finally we take a look at the effect that the number of splines $n$ has on the error. Of course, we expect that a higher number of splines yields better approximation results when the $\xi$-range stays constant. But is this really the case?
%In order to answer this question we are first going to look at four simulation outcomes for each Knudsen number that differ in the number of splines $n$ (see Figure \ref{kase} and \ref{resultsDifferentN}). Then we will consider the error evolution for $n=1, \hdots, 12$ in an error plot (see Figure \ref{errorPlotsDifferentOrders}).

%For the small Kundsen number $Kn=0.05$ we cannot tell that the approximation results improve for a larger number of splines with absolute certanity (see Figure \ref{kase}). The solution here is already relatively accurate and smooth. If we consider $Kn=0.5$, however, we can observe a clear evolution of the results (see Figure \ref{resultsDifferentN}). As always, the largest discrepancies occur for the velocity graph.
\begin{figure}[H]
\centering
%\begin{subfigure}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{maboGraphics/results/kase1}
%\caption{$n=4$.}
%\label{kase1}
%\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{maboGraphics/results/kase2}
\caption{$n=7$.}
\label{kase2}
\end{subfigure}
\hfill
%\hfill
%\begin{subfigure}[t]{0.49\textwidth}
%\centering
%\includegraphics[width=\textwidth]{maboGraphics/results/kase3}
%\caption{$n=10$.}
%\label{kase3}
%\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{maboGraphics/results/kase4}
\caption{$n=13$.}
\label{kase4}
\end{subfigure}
\caption{Shock tube for different $n$, $[\xi_{min},\xi_{max}]=[-4,4]$, $\textrm{Kn}=0.05$, $k=1$.}
\label{kase}
\end{figure}

\begin{figure}[H]
 \centering
% \begin{subfigure}[t]{0.49\textwidth}
% \centering
% \includegraphics[width=\textwidth]{maboGraphics/results/count1}
% \caption{$n=4$.}
% \label{count1}
%\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/count2}
 \caption{$n=7$.}
 \label{count2}
\end{subfigure}
\hfill
%\hfill
%\begin{subfigure}[t]{0.49\textwidth}
% \centering
% \includegraphics[width=\textwidth]{maboGraphics/results/count3}
% \caption{$n=10$.}
% \label{count3}
%\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/count4}
 \caption{$n=13$.}
 \label{count4}
\end{subfigure}
\caption{Shock tube for different $n$, $[\xi_{min},\xi_{max}]=[-4,4]$, $\textrm{Kn}=0.5$, $k=1$.}
\label{resultsDifferentN}
\end{figure}

In the error plots in Figure \ref{errorPlotsDifferentOrders} the relative errors are defined as
%\begin{equation}
% \begin{split}
%  \err_{\rho}&=\frac{\int | \rho_{SME}-\rho_{sol} | dx }{ \int | \rho_{sol} | dx}, \\
%\err_{p}&=\frac{\int | p_{SME}-p_{sol} | dx }{ \int | p_{sol} | dx},\\
%\textrm{{and}} \: \err_{v}&=\frac{\int | v_{SME}-v_{sol} | dx }{ \int | v_{sol} | dx}.
% \end{split}
%\end{equation}
\begin{equation}
  \err_{\rho}=\frac{\int | \rho_{SME}-\rho_{sol} | dx }{ \int | \rho_{sol} | dx},
\err_{p}=\frac{\int | p_{SME}-p_{sol} | dx }{ \int | p_{sol} | dx},
\textrm{{and}} \: \err_{v}=\frac{\int | v_{SME}-v_{sol} | dx }{ \int | v_{sol} | dx},
\end{equation}
and the error is plotted for different spline orders $k=1,2,3$ in each figure. Additionally, we consider the error evolution on different $\xi$-ranges $[\xi_{min},\xi_{max}]=[-3,3]$ and $[\xi_{min},\xi_{max}]=[-4,4]$.

In all settings the error for density and pressure is a factor 10 smaller than the velocity error in accordance with the literature \cite{Koellermeier2017b}. The errors clearly decrease with increasing number of splines. For about $10$ basis functions, the error can be reduced up to $0.2\%-0.4\%$ for $\rho$ and $p$ and to around $4\%$ for $v$, which is very accurate when considering the small number of equations involved. For $[\xi_{min},\xi_{max}]=[-2,2]$ the $\xi$-range is too small and no convergence is obtained (not shown here). A higher spline order leads to a slightly smaller error while showing the same trend as first order.

%The effect of the spline orders cannot really be deduced from our results. However, the plots in Figure \ref{error4} and Figure \ref{error6} might indicate that the error decline is sharper for splines of higher order, similar to what we saw in the previous chapter for the known distribution functions.
}
\begin{figure}[H]
 \centering
% \begin{subfigure}[t]{0.47\textwidth}
% \centering
% \includegraphics[width=\textwidth]{maboGraphics/results/error2}
% \caption{$[\xi_{min},\xi_{max}]=[-2,2]$, $k=1,2,3$ {(from bright to dark)}.}
% \label{error2}
%\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/error4}
 \caption{$[\xi_{min},\xi_{max}]=[-3,3]$, $k=1,2,3$ {(from bright to dark)}.}
 \label{error4}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/error6}
 \caption{$[\xi_{min},\xi_{max}]=[-4,4]$, $k=1,2,3$ {(from bright to dark)}.}
 \label{error6}
\end{subfigure}
%\caption{{Relative error for different $n$} $[\xi_{min},\xi_{max}]$=$\{[-2,2]$, $[-3,3]$, $[-4,4]\}$.}
\caption{Simulation error depending on $n$ for different orders, $[\xi_{min},\xi_{max}]$, $\textrm{Kn}=0.5$.}
\label{errorPlotsDifferentOrders}
\end{figure}


\subsubsection{Comparison with existing QBME model}
In comparison with the reference solution obtained by a discrete velocity method, our method yields a fast and accurate solution. Now we compare the new SME with the existing Quadrature-Based Moment Equations (QBME), e.g. as described in \cite{Koellermeier2017b}. We compare using the same number of equations in Figure \ref{hermiteCmp} and we observe that the results look surprisingly similar and even deviations from the reference solution occur in similar positions. Both methods are very accurate for the case $\textrm{Kn}=0.05$. In Figure \ref{hermite4} the spline method appears to be more accurate. The SME model thus yields more accurate solutions than the existing moment model in this test case.
\begin{figure}[H]
 \centering
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/hermite1}
 \caption{$n=5$, $\textrm{Kn}=0.05$.}
 \label{hermite1}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/hermite2}
 \caption{$n=5$, $\textrm{Kn}=0.5$.}
 \label{hermite2}
\end{subfigure}
\hfill
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/hermite3}
 \caption{$n=11$, $\textrm{Kn}=0.05$.}
 \label{hermite3}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/hermite4}
 \caption{$n=11$, $\textrm{Kn}=0.5$.}
 \label{hermite4}
\end{subfigure}
\caption{Shock tube comparing SME to QBME model. $k=1$, $[\xi_{min},\xi_{max}]=[-4,4]$.}
\label{hermiteCmp}
\end{figure}

Summarizing the shock tube test case, we saw that the new SME model approximates the solution with good accuracy and even outperforms the existing QBME model for the same number of equations.


\subsection{Symmetric two-beam problem}
In this test case from \cite{Schaerer2015}, a BGK collision operator with a constant relaxation time $\tau = \textrm{Kn} \cdot t_{\textrm{END}}$ is used to model collisions similar to the shock tube test case.

The initial Riemann data is given by \eqref{e:2beam_IC}
\begin{equation}
    \vect{u}_M^L = \left( 1,0.5,1,0,\ldots,0\right)^T, \quad \quad \vect{u}_M^R = \left( 1,-0.5,1,0,\ldots,0\right)^T,
    \label{e:2beam_IC}
\end{equation}
and models two colliding Maxwellian distributed particle beams. We note that this test case is especially challenging for any type of polynomial ansatz as it is difficult to represent the analytical solution using a polynomial expansion. In the free streaming case $\textrm{Kn} = \infty$, the analytical solution is a sum of two Maxwellians distributions according to \cite{Schaerer2015}.

The numerical tests are performed on the computational domain $[-10,10]$, discretized using $4000$ points. The end time is $t_{\textrm{END}}=0.3$ using a constant CFL number of approximately $0.5$ for all tests.

Tests are shown for $\textrm{Kn} = 0.1$ representing a small Knudsen number, $\textrm{Kn} = 1$ for a relatively large Knudsen number and $\textrm{Kn} = \infty$ leading to vanishing right-hand side and very sharp profiles. We show results for the SME model using $n=10$ with order $k=1$, and $[\xi_{min},\xi_{max}]=[-4,4]$, which was identified as accurate in the previous shock tube test case. The results are similar for other SME models using more equations, but we omit the results here for conciseness. We use the hyperbolic QBME model with $M+1=10$ equations for comparison. We note that the QBME model yields similar results as the HME model from \cite{Cai2013} for this test case. A discrete velocity method is used as reference solution. It was taken from \cite{Schaerer2015} and computed using 2000 cells in physical space and 600 variables for the discretization of the microscopic velocity space. Note that the DVM method is computationally much more expensive in comparison to the lower-dimensional moment models. As in the literature, we display pressure $p=\rho\theta$ and the normalized heat flux $\bar{q}$ (see \ref{e:Q}), which can be computed for the moment models using
\begin{equation} \label{e6:normalized_heat_flux}
    \bar{q} = \frac{6 f_3}{\rho \sqrt{\theta}^3},
\end{equation}
while in the case of this specific spline model, we derive the heat flux to
%\begin{equation} \label{e6:normalized_heat_flux}
%    %\bar{q} = 0.00699159 \kappa_1 + 0.179474 \kappa_2 + 0.179474 \kappa_3 + 0.00699159 \kappa_4.
%    \bar{q} = 0.000847777 \kappa_1 + 0.0129408 \kappa_2 +  0.0836094 \kappa_3 + 0.170286 \kappa_4 + 0.0836094 \kappa_5 + 0.0129408 \kappa_6 + 0.000847777 \kappa_7.
%\end{equation}
\begin{equation}
    \begin{split}
        \label{e6:normalized_heat_flux10}
        \bar{q} \approx 0.000847777 \kappa_1 + 0.0129408 \kappa_2 +  0.0836094 \kappa_3 + 0.170286 \kappa_4 \\
         + 0.0836094 \kappa_5 + 0.0129408 \kappa_6 + 0.000847777 \kappa_7.
    \end{split}
\end{equation}

The results in Figure \ref{2beam} show a clear convergence of the new SME model and at least similar accuracy in comparison to the QBME model, which uses the same number of equations. For the collisionless test case with $\textrm{Kn} = \infty$, the step-like structure is due to the wave structure of the problem with at most $n=10$ propagation speeds, see remark \ref{remark_hyp}. We can see that the propagation speeds are slightly different than those of the QBME model, because of the different ansatz. For $\textrm{Kn} = 1$, the solution gets closer to the DVM reference, while still showing a similar accuracy to the QBME model. For the smallest Knudsen number $\textrm{Kn} = 0.1$, the new SME model has converged to the reference solution. Notably, also the heat flux is approximated with high precision.
\begin{figure}[H]
 \centering
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10Kninftyp}
 \caption{$Kn = \infty$, $p$}
\end{subfigure}
\hfill
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10KninftyQ}
 \caption{$Kn = \infty$, $\bar{q}$}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10Kn1p}
 \caption{$Kn = 1$, $p$}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10Kn1Q}
 \caption{$Kn = 1$, $\bar{q}$}
\end{subfigure}
\hfill
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10Kn0p1p}
 \caption{$Kn = 0.1$, $p$}
\end{subfigure}\hfill
 \begin{subfigure}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10Kn0p1Q}
 \caption{$Kn = 0.1$, $\bar{q}$}
\end{subfigure}
\caption{Symmetric two-beam result for SME and $n=10$. QBME for comparison and DVM for reference.}
\label{2beam}
\end{figure}

To better quantify the convergence of the new model, we plot the errors with decreasing Knudsen number in Figure \ref{2beamconv}. We clearly see that the error of the new SME model is smaller than the known QBME model for both the velocity $u$ and pressure $p$. For the heat flux, the error is of the same order, whereas the QBME model is slightly more accurate for smaller Knudsen number.

We summarize that the SME model successfully approximates the solution of the symmetric two-beam problem for different Knudsen numbers. For most cases, the model outperforms the, already accurate, existing QBME model.

\begin{figure}[H]
 \centering
 \begin{subfigure}[t]{0.3\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10convu}
 \caption{$u$}
\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.3\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10convp}
 \caption{$p$}
\end{subfigure}
\begin{subfigure}[t]{0.3\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/2beam/SME10convQ}
 \caption{$\bar{q}$}
\end{subfigure}
\caption{Error convergence of symmetric two-beam simulations using $n=10$.}
\label{2beamconv}
\end{figure}


\subsection{Shock structure problem} \label{sec:Shock_structure}
For the last test case, the stationary shock structure problem, we again closely follow the descriptions in \cite{Koellermeier2017b,Schaerer2015}. We choose given upstream and downstream boundary conditions from the Rankine-Hugoniot conditions \cite{Ruggeri1993}. Setting the Mach number $\mathrm{Ma} = 1.8$ leads to
\begin{equation}
    \begin{aligned}
        \rho_L &= 1, &
        \rho_R &= \rho_L \cdot \frac{2 \mathrm{Ma}^2}{\mathrm{Ma}^2+1} \approx 1.528,\\
        u_L &= \sqrt{3} \, \mathrm{Ma} \approx 3.118, \qquad&
        \vel_R &= \vel_L \cdot \frac{\mathrm{Ma}^2+1}{2 \mathrm{Ma}^2} \approx 2.040,\\
        \theta_L &= 1,&
        \theta_R &= \theta_L \cdot \frac{\left(1+\mathrm{Ma}^2\right)\left(3\mathrm{Ma}^2-1\right)}{4\mathrm{Ma}^2} \approx 2.853.
    \end{aligned}
\end{equation}

For the relaxation time on the right-hand side collision operator, a constant $\tau=0.01$ is used.
The steady-state solution is computed by time marching from the discontinuous initial data until
convergence. Afterwards the density is scaled using
\begin{equation}\label{e:BC}
     \widetilde{\rho} = \frac{\rho-\rho_L}{\rho_R-\rho_L} \Rightarrow \widetilde{\rho} \in [0,1]
\end{equation}
and the shock positions are aligned at $x=0$ by matching the values of $\widetilde{\rho} = 1/2$.
The computational grid is $[x_L,x_R]=[-78,78]$ using $N_x~=~7500$ cells with a CFL number of
approximately $0.5$. We will use the SME model with $n=7$ equations to match the number of equations from the test case done in \cite{Koellermeier2017b} and use the order $k=1$ and $[\xi_{min},\xi_{max}]=[-4,4]$, similar as before.
The reference DVM solution from \cite{Mieussens2000} includes $N_v=400$ velocity
points and $N_x = 4000$ spatial cells. The reference method is thus far more expensive to compute in comparison to the models used here. We furthermore compare with the QBME model mentioned before and the HME model \cite{Cai2013}, which is widely used in the literature. To allow for a fair comparison, we use $M+1=7$ variables for the other moment models QBME and HME as well. Note that the highly non-linear QBME and HME are based on global basis functions in comparison to the bounded support of the splines used for SME.

We consider density $\rho$, velocity $u$, pressure $p=\rho\theta$, and the normalized heat flux $\bar{q}$ from \ref{e6:normalized_heat_flux} and for our SME model here computed by
\begin{equation} \label{e6:normalized_heat_flux7}
    \bar{q} \approx 0.00699159 \kappa_1 + 0.179474 \kappa_2 + 0.179474 \kappa_3 + 0.00699159 \kappa_4.
\end{equation}

The results in Figure \ref{fig:shockStructure} show that there are only very small differences between the standard hyperbolic moment models QBME, HME and the new SME model. The density, velocity and pressure plots show a good approximation property of the SME model, despite the fact that only $n=7$ variables are used. For the normalized heat flux $\bar{q}$, slight differences with respect to QBME and HME can be seen. However, the differences are of the order of the differences between the hyperbolic QBME and HME themselves. There is no significant additional deviation from the reference solution for the SME model.

The relative error comparison in Table \ref{tab:shockStructureErrors} shows that SME model yields a good approximation quality in comparison with the other models. The error of the new SME model is indeed significantly smaller than the error of the HME model on the one hand. The SME results in approximately the same error as the QBME model on the other hand. The surprising result is that the use of only very few spline functions can compete and even outperform some of the hyperbolic moment models.

\begin{figure}[htb]
 \centering
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/shockStructure/SME_rho_1x7500dt0p00002t1}
 \caption{$\rho$}
 \label{fig:shockStructure_rho}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/shockStructure/SME_u_1x7500dt0p00002t1}
 \caption{$u$}
 \label{fig:shockStructure_u}
\end{subfigure}
\hfill
 \begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/shockStructure/SME_p_1x7500dt0p00002t1}
 \caption{$p$}
 \label{fig:shockStructure_p}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=\textwidth]{maboGraphics/results/shockStructure/SME_Q_1x7500dt0p00002t1}
 \caption{$\bar{q}$}
 \label{fig:shockStructure_Q}
\end{subfigure}
\caption{Shock structure result for SME and $n=7$. QBME, HME for comparison and
DVM for reference.}
\label{fig:shockStructure}
\end{figure}

\begin{table}
\centering
\caption{Shock structure error comparison for different models.}
\label{tab:shockStructureErrors}
\tabcolsep7pt\begin{tabular}{c|c|c|c|c}
    model & $\rho$ & $\vel$ & $p$ & $Q$ \\
\hline
    SME & 1.09\% & 0.20\% & 0.59\% & 14.03\% \\
    QBME & 0.91\% & 0.19\% & 0.58\% & 12.26\% \\
    HME & 1.65\% & 0.32\% & 0.97\% & 16.22\%
\end{tabular}
\end{table}
